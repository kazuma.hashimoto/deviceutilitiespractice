#ifndef TIMEANDDATESETTINGS_H
#define TIMEANDDATESETTINGS_H

#include <QObject>
#include <QDebug>
#include <QtTimeDateSettings/QtTimeDateSettings>
#include <QtTimeDateSettings/systemtime.h>
#include <QtTimeDateSettings/timezonefiltermodel.h>
#include <QtTimeDateSettings/timezonemodel.h>
#include <QByteArray>
#include <QString>

class TimeAndDateSettings : public QObject
{
    Q_OBJECT
public:
    explicit TimeAndDateSettings(QObject *parent = nullptr);

signals:

public:
    void createTimeZoneItem();
    void timeZone();
};

#endif // TIMEANDDATESETTINGS_H
