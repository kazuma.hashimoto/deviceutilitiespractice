#ifndef NETWORKSETTINGS_H
#define NETWORKSETTINGS_H

#include <QObject>
#include <QStringListModel>
#include <QStringList>
#include <QDebug>
#include <QProcess>
#include <QByteArray>
#include <QVector>
#include <QTextStream>
#include <QThread>
#include <QtNetworkSettings/QNetworkSettingsManager>
#include <QtNetworkSettings/QNetworkSettingsState>
#include <QtNetworkSettings/QNetworkSettingsWireless>
#include <QtNetworkSettings/QNetworkSettingsService>
#include <QtNetworkSettings/QNetworkSettingsServiceModel>


class NetworkSettings : public QObject
{
    Q_OBJECT
public:
    explicit NetworkSettings(QObject *parent = nullptr);
    void initiate();

private:


};

#endif // NETWORKSETTINGS_H
