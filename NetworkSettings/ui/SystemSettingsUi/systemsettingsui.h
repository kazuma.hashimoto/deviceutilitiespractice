#ifndef SYSTEMSETTINGSUI_H
#define SYSTEMSETTINGSUI_H

#include <QWidget>
#include <QMessageBox>
#include <../src/NetworkSettings/networksettings.h>

namespace Ui {
class SystemSettingsUi;
}

class SystemSettingsUi : public QWidget
{
    Q_OBJECT

public:
    explicit SystemSettingsUi(QWidget *parent = nullptr);
    ~SystemSettingsUi();

private:
    Ui::SystemSettingsUi *ui;
    NetworkSettings _NetworkSettings;

    //slots from ui signals
private slots:
    void on_pushButton_home_clicked();
    void on_pushButton_connect_clicked();
    void on_comboBox_currentIndexChanged(const QString &arg1);
    void on_radioButton_wifi_clicked();
    void on_radioButton_enet_clicked();

private slots:
    //slots from networksettings
    void slotPushButton(bool &set2conn);
    void slotRadioButton(RadioButtonSetting &radioSetting);
    void slotMessageBox(MessageBoxType &messType);
    void slotUpdateCB(QStringListModel *currentSSIDs);
    void slotUpdateNetworkInfo(QStringList &networkInfo);
    void slotEnvSettings();
    void slotReadComboBox(QString &readSSID);

private:
    //message boxes
    void messageboxMissingPw();
    void messageboxWiFiEnabled();
    QMessageBox::StandardButton messageboxWiFiEnableQ();
    void messageboxNoEthConnection();
    void messageboxNoRBSelection();
    void messagboxEnvironmentSettings();
    void messageboxUnableToConnect();


signals:
    void signalHomeClicked();
};

#endif // SYSTEMSETTINGSUI_H
