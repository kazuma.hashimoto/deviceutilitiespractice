#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <../src/NetworkSettings/networksettings.h>
#include <../src/TimeAndDateSettings/timeanddatesettings.h>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    NetworkSettings _NetworkSettings;
    TimeAndDateSettings _TimeDateSettings;


private slots:
    void on_pushButton_close_clicked();
    void on_pushButton_Settings_clicked();
    void on_pushButton_initSetup_clicked();
    void slotMoveHome();

};
#endif // MAINWINDOW_H
