#include "mainwindow.h"
#include "./ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    _NetworkSettings.initiate();
    _TimeDateSettings.timeZone();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_close_clicked()
{
    this->close();
}

void MainWindow::slotMoveHome()
{
}

void MainWindow::on_pushButton_Settings_clicked()
{
}

void MainWindow::on_pushButton_initSetup_clicked()
{

}
